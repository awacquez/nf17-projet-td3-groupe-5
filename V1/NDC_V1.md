# Note de Clarification - Système de gestion d'une bibliothèque  

## Acteurs du Projet

__Client__ : Alessandro Victorino  

__Equipe Projet__ :  
- Korantin TOCZE
- Arthur WACQUEZ
- Lucas AUBARD

## Livrables
- README 
- Note de Clarification
- Modèle UML (MCD)
- Modèle logique (MLD relationnel)
- Code de la BDD : tables et vues, données de test

## Objets gérés
- Ressource
- Contributeur
- Compte
- Personnel
- Adhérent
- Adhésion 
- Prêt
- Sanction

NB : Un objet réservation pourra également être ajouté et permettra à un adhérent d'éffectuer une réservation en ligne.

## Propriétés des objets
 - Ressources : code, titre, liste de contributeurs, date d’apparition, éditeur, genre, code de classification, état (neuf, bon, abîmé ou perdu), disponible(oui,non). Une ressource peut être:    
-un Livre : ISBN, résumé, langue  
-un Film : langue, longueur, synopsis  
-un Enregistrements musicaux : longueur
- Contributeur : nom, prénom, date de naissance, nationalité. Un contributeur peut être:  
-Compositeur  
-Interprète  
-Réalisateur  
-Acteur  
-Auteur
 - Compte : login, mot de passe
 - Personne : nom, prénom, adresse, adresse email. Une personne peut être :  
   -Adhérent : date de naissance, numéro de téléphone, nombre de ressources actuellement empruntées, blacklisté, droit d'emprunt  
   -Membre du personnel 
- Adhésion : adhérent, date de début d’adhésion, date de fin d’adhésion 
-  Prêt : date de prêt, durée max du prêt, date retour du prêt, etat début, état fin
-  Sanction: adhérent, type de problème, ressource concernée

NB :   
- Date de retour du prêt et état fin sont actualisés lors du retour de la ressource
- Droit d'emprunt est modifié suivant le retard de retour d'une ressource.  
- Blacklisté est modifié suivant les sanctions d'un utilisateur donné.   
  
## Contraintes entre les objets et les propriétés :
- Une ressource est soit un livre, soit un film soit un enregistrement musical. 
- Une ressource dispose d'un code unique
- Un contributeur est soit réalisateur, un acteurs, un interprète, un compositeur ou un auteur.
- Les contributeurs d’un livre sont forcément des auteurs.
- Les contributeurs d’un film sont forcément des acteurs et/ou des réalisateurs.
- Les contributeurs d’un enregistrement musical sont forcément des compositeurs et/ou des interprètes.
- Une personne est soit un adhérent, soit un personnel.
- Un adhérent ne peut avoir qu’un nombre limité d’emprunts simultanés.
- Un adhérent ne peut pas emprunter de ressources si il ne dispose pas du droit d'emprunt.
- Un adhérent ne peut pas consulter ou emprunter des oeuvres si il est blacklisté.
- La date de retour de prêt doit être supérieure à la date de prêt.

## Liste des fonctions que les utilisateurs pourront effectués

 - Administrateur système   
    Gestion de l'ensemble des tables  
 - Membre du personnel   
    Gestion des Ressources, Contributeurs, Adhérents et Adhésion et Comptes (Lecture et Ecriture)   
    Gestion des Prêts et Sanctions (Peut entrer un prêt, et attribuer des sanctions)
 - Adhérents   
    Accès en lecture à Ressources, Contributeurs et ses Prêts à lui  
    Modification du mot de passe de son Compte  
    
 
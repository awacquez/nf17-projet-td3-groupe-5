/*
Ce document contient le code SQL afin de générer des vues pour vérifier les contraintes logiques.
Ces contraintes seront gérées par la suite au niveau applicatif (non geré dans ce projet)
Par exemple, lors de l'ajout d'une ressource, les vues Code_Union et Verif_ressource seront crees.
Si les deux ne sont pas egales, alors l'ajout de ressource sera annulé
*/

/* Contraintes sur la relation ressource et ses filles
    1- Union(Projection(EnregMusic, code),Projection(Livre, code),Projection(Film, code)) = Projection(Ressource, code)
    Tous les tuples doivent se situer dans une des classes filles : classe abstraite
    On doit avoir Verif_Ressource=NULL
*/


CREATE OR REPLACE VIEW Code_union AS
SELECT E.code AS Un FROM EnregMusic E
UNION 
SELECT L.code FROM Livre L
UNION
SELECT F.code FROM Film F;

CREATE OR REPLACE VIEW Verif_Ressource AS
SELECT Code FROM Ressource
EXCEPT 
SELECT Un AS code FROM Code_union;


-- 2- Intersection(Projection(EnregMusic, code),Projection(Livre, code),Projection(Film, code)= {}
--    On doit avoir Intersect_Ressource=NULL
CREATE OR REPLACE VIEW Intersect_Ressource AS
SELECT E.code AS En FROM EnregMusic E
INTERSECT
SELECT L.code AS Li FROM Livre L
INTERSECT
SELECT F.code AS Fi FROM Film F;


/* Contraintes de cardinalité sur ContribMusic :
  Projection(Role,nom,prenom,DDN,metier) = Projection(ContributionMusic,nom,prenom,DDN,metier) AND Projection(EnregMusic,code) = Projection(ContributionMusic,codeMusic)
  Donc les vues suivantes doivent être vides.
*/

CREATE OR REPLACE VIEW Verif_ContribMusic AS
SELECT R.nom,R.prenom,R.DDN,R.metier FROM Roles R
WHERE Metier ='Interprete'
OR Metier ='Compositeur'
EXCEPT
SELECT nom,prenom,ddn,metier FROM ContribMusic;

CREATE OR REPLACE VIEW Verif_ContribMusic2 AS
SELECT code FROM EnregMusic
EXCEPT
SELECT code FROM ContribMusic;

/* Contraintes de cardinalité sur ContribLivre :
  PROJECTION(Role,nom,prenom,DDN,metier) = PROJECTION(ContributionLivre,nom,prenom,DDN,metier) AND PROJECTION(Livre,code) = PROJECTION(ContributionLivre,codeLivre)
  Donc les vues suivantes doivent être vides.
*/

CREATE OR REPLACE VIEW Verif_ContribLivre AS
SELECT R.nom,R.prenom,R.DDN,R.metier FROM Roles R
WHERE Metier ='Auteur'
EXCEPT
SELECT nom,prenom,ddn,metier FROM ContribLivre;

CREATE OR REPLACE VIEW Verif_ContribLivre2 AS
SELECT code FROM Livre
EXCEPT
SELECT code FROM ContribLivre;

/* Contraintes de cardinalité sur ContribFilm :
  Projection(Role,nom,prenom,DDN,metier) = Projection(ContributionFilm,nom,prenom,DDN,metier) AND Projection(Film,code) = Projection(ContributionFilm,codeFilm)
  Donc les vues suivantes doivent être vides.
*/

CREATE OR REPLACE VIEW Verif_ContribFilm AS
SELECT R.nom,R.prenom,R.DDN,R.metier FROM Roles R
WHERE Metier ='Acteur'
OR Metier ='Realisateur'
EXCEPT
SELECT nom,prenom,ddn,metier FROM ContribFilm;

CREATE OR REPLACE VIEW Verif_ContribFilm2 AS
SELECT code FROM Film
EXCEPT
SELECT code FROM ContribFilm;

/* Contraintes de cardinalité sur Ressource et Exemplaire :
   Projection(Ressource,code)=Projection(Exemplaire,code)
   La vue VerifExemplaire doit être vide
*/

CREATE OR REPLACE VIEW Verif_Exemplaire AS
SELECT code FROM RESSOURCE
EXCEPT 
SELECT code FROM EXEMPLAIRE

/* Contrainte de cardinalité sur Roles et Contributeurs :
   Projection(Roles,nom,prenom,ddn)=Projection(Contributeurs,nom,prenom,ddn)
*/

CREATE OR REPLACE VIEW Verif_Roles AS
SELECT nom,prenom,ddn FROM ROLES
EXCEPT
SELECT nom,prenom,ddn FROM CONTRIBUTEURS

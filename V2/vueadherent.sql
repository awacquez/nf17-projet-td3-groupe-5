/*
Ce document contient le code SQL afin de générer des vues pour l'adherent
Ces vues correspondent aux fonctions de base que peuvent effectuer un adherent
Elles peuvent etre par exemple, consulter les differentes oeuvres correspondant a un genre particulier ou obtenir la liste des ressources empruntables ou non
La liste des vues n'est pas exhaustive, mais constitue un exemple des fonctions utilisable par l'adherent qui possederait une application (non developpe dans notre cas)
Dans tous les exemples ou on attend une reponse precise (par exemple connaitre les oeuvres selon UN genre), une valeur correspond au jeu de donnée est précisee (afin de pouvoir executer le code)
Il suffit de modifier cette valeur afin de tester differentes possibilites
*/

-- Afficher toutes les oeuvres du genre 'Action'

CREATE OR REPLACE VIEW OeuvreGenre AS
SELECT titre,editeur,'Livre' as type
FROM Ressource R,Livre L
WHERE R.code=L.code
AND genre='Action'
UNION
SELECT titre,editeur,'Film' as type
FROM Ressource R,Film F
WHERE R.code=F.code
AND genre='Action'
UNION
SELECT titre,editeur,'Musique' as type
FROM Ressource R,EnregMusic as M
WHERE R.code=M.code
AND genre='Action';

-- Afficher toutes les oeuvres ou 'Tarantino' a contribue
CREATE OR REPLACE VIEW ContributionPersonne AS
SELECT R.titre,R.editeur, C.nom, 'Livre' as type
FROM RESSOURCE R, LIVRE L,CONTRIBLIVRE C
WHERE R.code=L.code
AND L.code=C.code
AND C.nom='Tarantino'
UNION
SELECT R.titre,R.editeur, C.nom, 'Film' as type
FROM RESSOURCE R, FILM F,CONTRIBFILM C
WHERE R.code=F.code
AND F.code=C.code
AND C.nom='Tarantino'
UNION
SELECT R.titre,R.editeur, C.nom, 'Musique' as type
FROM RESSOURCE R, ENREGMUSIC M,CONTRIBMUSIC C
WHERE R.code=M.code
AND M.code=C.code
AND C.nom='Tarantino';

-- Afficher tous les exemplaires disponibles (donc empruntables)
CREATE OR REPLACE VIEW ExemplairesDispo AS
SELECT R.titre,E.code,E.num_exemplaire
FROM RESSOURCE R,EXEMPLAIRE E
WHERE R.code=E.code
AND E.etat!='Mauvais'
AND E.etat!='Perdu'
EXCEPT
SELECT R.titre,E.code,E.num_exemplaire
FROM EXEMPLAIRE E,PRET P,RESSOURCE R
WHERE R.code=E.code
AND E.code=P.code
AND E.num_exemplaire=P.num_exemplaire
AND P.dateretour IS NULL;

-- Afficher les emprunts d'un adherent (ici, l'adherent connecte est 'aubardma')
CREATE OR REPLACE VIEW Prets_en_cours AS
SELECT R.Titre,R.editeur,R.code, T.num_exemplaire
FROM Pret T,Ressource R,Personne P
WHERE T.adherent=P.login
AND T.code=R.code
AND T.dateretour IS NULL
AND T.adherent='aubardma';
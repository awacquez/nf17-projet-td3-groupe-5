/*
Ce document contient les codes SQL afin de générer des vues correspondant à des méthodes
En effet, nous avions a la base créé plusieurs attributs qui constituaient des redondances d'informations, car ils pouvaient être calculés via d'autres tables.
Par exemple, une variable booléenne droit d'emprunt (il suffit de vérifier si l'utilisateur possède un prêt en retard), ou le nombre d'emprunts en cours.
Nous avons remplace ces attributs par des vues
Les vues genérées dans ce code seront ensuite utilisées au niveau applicatif (que nous ne gérons pas dans notre cas). 
Par exemple, l'application générera la vue qui affiche le nombre de prêts en retard lors d'une demande d'ajout dans la table pret
Si l'utilisateur n'est pas present dans la vue, alors l'utilisateur peut emprunter, et le pret est ajouté a la table
*/

-- Afficher les adhérents en cours :
-- Verifier qu'un adherent est actuellement adherent lors d'un pret par exemple. La vue affiche tous les adherent en cours.
-- Il faut verifier que l'adherent qui souhaite faire un pret appartient a la vue)

CREATE OR REPLACE VIEW Adherent_en_cours AS
SELECT P.login,P.nom,P.prenom,B.datedebut,(B.dateDebut+'1 YEAR'::interval)::date AS Datefin
FROM PERSONNE P, ADHERENT A, ADHESION B
WHERE P.login=A.login
AND A.login=B.adherent
AND (B.dateDebut+'1 YEAR'::interval) >= CURRENT_DATE;


--Determine les exemplaires disponibles :
-- La vue affiche tous les exemplaires disponibles
-- Il faut verifier qu'un exemplaire d'une oeuvre est disponible (donc present dans la vue), afin de savoir si un pret est possible.

CREATE OR REPLACE VIEW ExemplairesDispo AS
SELECT R.titre,E.code,E.num_exemplaire
FROM RESSOURCE R,EXEMPLAIRE E
WHERE R.code=E.code
AND E.etat!='Mauvais'
AND E.etat!='Perdu'
EXCEPT
SELECT R.titre,E.code,E.num_exemplaire
FROM EXEMPLAIRE E,PRET P,RESSOURCE R
WHERE R.code=E.code
AND E.code=P.code
AND E.num_exemplaire=P.num_exemplaire
AND P.dateretour IS NULL;

--Affichage du nombre de prêts en retard :
-- Un utilisateur qui a un pret en retard ne peut pas emprunter. La vue affiche le nombre de pret en retard par utilisateur
-- Il faut verifier que l'utilisateur n'est pas present dans la vue (donc n'a pas de pret en retard) pour qu'il puisse emprunter

CREATE OR REPLACE VIEW Pret_en_retard AS
SELECT P.Nom,P.Prenom,P.login,COUNT(R.titre) as NombreRetard
FROM Pret T,Ressource R,Personne P
WHERE T.adherent=P.login
AND T.code=R.code
AND (T.datepret+ '1 MONTH'::interval)<CURRENT_DATE
AND T.dateretour IS NULL
GROUP BY P.nom,P.prenom,P.login


--Nombre de prets en cours d'un utilisateur :
-- Un utilisateur qui a plus de 5 prets en cours ne peut pas emprunter. La vue affiche le nombre de prets en cours par utilisateur
-- Il faut verifier que l'utilisateur possede moins de 5 prets dans la vue pour qu'il puisse emprunter

CREATE OR REPLACE VIEW Nb_Prets_en_cours AS
SELECT  P.login,P.Nom,P.Prenom,COUNT(T.datepret) FROM Pret T,Ressource R,Personne P
WHERE T.dateretour IS NULL
AND T.adherent=P.login
AND T.code=R.code
GROUP BY (P.login,P.nom,P.Prenom);
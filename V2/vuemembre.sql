/*
Ce document contient le code SQL afin de générer des vues pour les membres
Ces vues correspondent aux fonctions que peuvent effectuer un membre du personnel
Elles peuvent etre par exemple, afficher le nombre de sanctions par utilisateur, afficher les adhesions en cours ou passees. 
La liste des vues n'est pas exhaustive, mais constitue un exemple des fonctions utilisable par l'adherent qui possederait une application (non developpe dans notre cas)
Dans tous les exemples ou on attend une reponse precise (par exemple connaitre les sanctions pour UN adherent), une valeur correspond au jeu de donnée est précisee (afin de pouvoir executer le code)
Il suffit de modifier cette valeur afin de tester differentes possibilites.
Ce document contient également les fonctions statistiques : elles permettent d'afficher le nombre d'emprunt par type d'oeuvre ou par genre. 
Ces vues permettent aux membres du personnel d'établir une liste de documents populaires, et également d'envisager la suggestion de documents aux adhérents

*/

-- Consultation des adhesions en cours
CREATE OR REPLACE VIEW AdhesionsActuelles AS
SELECT P.login,P.nom,P.prenom,B.datedebut,(B.dateDebut+'1 YEAR'::interval)::date AS Datefin
FROM PERSONNE P, ADHERENT A, ADHESION B
WHERE P.login=A.login
AND A.login=B.adherent
AND (B.dateDebut+'1 YEAR'::interval) >= CURRENT_DATE;

-- Consultation des adhesions passees
SELECT P.login,P.nom,P.prenom,B.datedebut,(B.dateDebut+'1 YEAR'::interval)::date AS Datefin
FROM PERSONNE P, ADHERENT A, ADHESION B
WHERE P.login=A.login
AND A.login=B.adherent
AND (B.dateDebut+'1 YEAR'::interval)< CURRENT_DATE;

-- Consultation du nombre de sanctions par adherent
CREATE OR REPLACE VIEW SanctionsAdherents AS
SELECT P.login,P.nom,P.prenom,count(*)
FROM PERSONNE P,SANCTION S
WHERE P.login=S.adherent
GROUP BY (P.login,P.nom,P.prenom);

-- Consultation du nombre de prêts en retard par adherent
SELECT P.Nom,P.Prenom,P.login,COUNT(R.titre) as NombreRetard
FROM Pret T,Ressource R,Personne P
WHERE T.adherent=P.login
AND T.code=R.code
AND (T.datepret+ '1 MONTH'::interval)<CURRENT_DATE
AND T.dateretour IS NULL
GROUP BY P.nom,P.prenom,P.login

--Fonctions statistiques : 
CREATE OR REPLACE VIEW Stat_Emprunt_Global AS
SELECT R.titre,COUNT(*) AS Nombre FROM Ressource R,Pret P
WHERE R.code=P.code
AND P.datepret > CURRENT_DATE - '1 YEAR'::interval
GROUP BY R.titre
ORDER BY COUNT(*) DESC;

CREATE OR REPLACE VIEW Stat_Emprunt_Music AS
SELECT R.titre,COUNT(*) AS Nombre FROM Ressource R,Pret P,EnregMusic E
WHERE R.code=P.code
AND R.code=E.code
AND P.datepret > CURRENT_DATE - '1 YEAR'::interval
GROUP BY R.titre
ORDER BY COUNT(*) DESC;

CREATE OR REPLACE VIEW Stat_Emprunt_Livre AS
SELECT R.titre,COUNT(*) AS Nombre FROM Ressource R,Pret P,Livre E
WHERE R.code=P.code
AND R.code=E.code
AND P.datepret > CURRENT_DATE - '1 YEAR'::interval
GROUP BY R.titre
ORDER BY COUNT(*) DESC;

CREATE OR REPLACE VIEW Stat_Emprunt_Film AS
SELECT R.titre,COUNT(*) AS Nombre FROM Ressource R,Pret P,Film E
WHERE R.code=P.code
AND R.code=E.code
AND P.datepret > CURRENT_DATE - '1 YEAR'::interval
GROUP BY R.titre
ORDER BY COUNT(*) DESC;

CREATE OR REPLACE VIEW Genre_Emprunt_Global AS
SELECT R.genre,COUNT(*) AS Nombre FROM Ressource R,Pret P
WHERE R.code=P.code
AND P.datepret > CURRENT_DATE - '1 YEAR'::interval
GROUP BY R.genre
ORDER BY COUNT(*) DESC;


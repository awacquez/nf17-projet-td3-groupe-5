-- Suppression des tables et types

DROP TABLE IF EXISTS Pret;
DROP TABLE IF EXISTS Adhesion;
DROP TABLE IF EXISTS Sanction;
DROP TABLE IF EXISTS Exemplaire;
DROP TABLE IF EXISTS Adherent;
DROP TABLE IF EXISTS Membre;
DROP TABLE IF EXISTS Personne;
DROP TABLE IF EXISTS ContribLivre;
DROP TABLE IF EXISTS ContribFilm;
DROP TABLE IF EXISTS ContribMusic;
DROP TABLE IF EXISTS Roles;
DROP TABLE IF EXISTS Contributeurs;
DROP TABLE IF EXISTS Film;
DROP TABLE IF EXISTS Livre;
DROP TABLE IF EXISTS EnregMusic;
DROP TABLE IF EXISTS Ressource;
DROP TYPE IF EXISTS Etat;
DROP TYPE IF EXISTS Metier;

-- Creation des types
CREATE TYPE Metier AS ENUM ('Auteur', 'Acteur', 'Realisateur','Compositeur','Interprete');
CREATE TYPE Etat AS ENUM ('Neuf', 'Bon', 'Mauvais','Perdu');

-- Creation des tables

CREATE TABLE Ressource(
    code SERIAL PRIMARY KEY,
    titre VARCHAR NOT NULL,
    date DATE,
    editeur VARCHAR,
    genre VARCHAR,
    code_Classification VARCHAR NOT NULL
);
CREATE TABLE EnregMusic(
    code INT REFERENCES Ressource(code),
    longueur INT NOT NULL,
    PRIMARY KEY(code)
);
CREATE TABLE Livre(
    code INT REFERENCES Ressource(code),
    ISBN VARCHAR NOT NULL,
    resume TEXT NOT NULL,
    langue VARCHAR NOT NULL,
    PRIMARY KEY(code)
);
CREATE TABLE Film(
    code INT REFERENCES Ressource(code),
    longueur INT NOT NULL,
    synopsis TEXT NOT NULL,
    langue VARCHAR NOT NULL,
    PRIMARY KEY(code)
);

CREATE TABLE Contributeurs(
     nom VARCHAR,
     prenom VARCHAR,
     DDN DATE,
     nationalite VARCHAR,
     PRIMARY KEY(nom,prenom,DDN)
);
CREATE TABLE Roles(
     nom VARCHAR,
     prenom VARCHAR,
     DDN DATE,
     metier metier,
     PRIMARY KEY(Nom,Prenom,DDN,Metier),
     FOREIGN KEY(nom,prenom,DDN) REFERENCES Contributeurs(nom,prenom,DDN)
);
CREATE TABLE ContribMusic(
     code INT,
     nom VARCHAR,
     prenom VARCHAR,
     DDN DATE,
     metier metier,
     PRIMARY KEY(code,nom,prenom,DDN,metier),
     FOREIGN KEY(code) REFERENCES ENREGMUSIC(code),
     FOREIGN KEY(nom,prenom,ddn,metier) REFERENCES Roles(nom,prenom,DDN,metier),
     CHECK ((metier='Compositeur') OR (metier='Interprete'))
);

CREATE TABLE ContribFilm(
     code INT,
     nom VARCHAR,
     prenom VARCHAR,
     ddn DATE,
     metier metier,
     PRIMARY KEY(code,nom,prenom,DDN,metier),
     FOREIGN KEY(code) REFERENCES FILM(code),
     FOREIGN KEY(nom,prenom,ddn,metier) REFERENCES Roles(nom,prenom,DDN,metier),
     CHECK ((metier='Acteur') OR (metier='Realisateur'))
);
CREATE TABLE ContribLivre(
     code INT,
     nom VARCHAR,
     prenom VARCHAR,
     DDN DATE,
     metier metier,
     PRIMARY KEY(code,nom,prenom,DDN,metier),
     FOREIGN KEY(code) REFERENCES LIVRE(code),
     FOREIGN KEY(nom,prenom,DDN,metier) REFERENCES Roles(nom,prenom,DDN,metier),
     CHECK (metier='Auteur')
);

CREATE TABLE Personne(
    login VARCHAR PRIMARY KEY,
    Nom VARCHAR,
    Prenom VARCHAR,
    Adresse VARCHAR,
    Adresse_mail VARCHAR,
    Mot_de_passe VARCHAR
);

CREATE TABLE Membre(
    login VARCHAR REFERENCES Personne(login),
    PRIMARY KEY(login)
);


CREATE TABLE Adherent(
    login VARCHAR REFERENCES Personne(login) PRIMARY KEY,
    DDN DATE NOT NULL,
    Numero_tel VARCHAR NOT NULL,
    blacklist BOOL NOT NULL
);

CREATE TABLE Exemplaire(
    code INTEGER REFERENCES Ressource(code), 
    num_exemplaire INTEGER NOT NULL ,
    etat ETAT,
    PRIMARY KEY (code,num_exemplaire)
);

 CREATE TABLE Sanction(
    code_ressource INTEGER,
    num_exemplaire INTEGER,
    adherent VARCHAR REFERENCES Adherent(login),
    date_sanction DATE,
    type VARCHAR,
    FOREIGN KEY(code_ressource,num_exemplaire) REFERENCES Exemplaire(code,num_exemplaire),
    PRIMARY KEY(code_ressource,num_exemplaire,adherent,date_sanction)
);

CREATE TABLE Adhesion(
    adherent VARCHAR REFERENCES Adherent(login),
    dateDebut DATE,
    dateFin DATE,
    PRIMARY KEY(adherent,dateDebut)
);

CREATE TABLE Pret(
    adherent VARCHAR REFERENCES ADHERENT(login),
    code INTEGER ,
    num_exemplaire INTEGER ,
    datePret DATE NOT NULL,
    dateRetour DATE,
    etatDebut ETAT NOT NULL,
    etatFin ETAT,
    FOREIGN KEY (code,num_exemplaire) REFERENCES Exemplaire(code,num_exemplaire),
    PRIMARY KEY (adherent,code,num_exemplaire,datePret),
    CHECK (((dateRetour >= datePret) AND (etatFin IS NOT NULL))
    OR ((dateRetour IS NULL) AND( etatFin IS NULL)))
);

-- Ajout des jeux de donnees

-- Ajout des contributeurs et de leur roles

INSERT INTO CONTRIBUTEURS VALUES('Mozart','Amadeus','1756-01-27','autrichien');
INSERT INTO CONTRIBUTEURS VALUES('Orwell','George','1903-06-25','britannique');
INSERT INTO CONTRIBUTEURS VALUES('Tarantino','Quentin','1963-03-27','americain');
INSERT INTO CONTRIBUTEURS VALUES('Dujardin','Jean','1972-06-19','français');
INSERT INTO CONTRIBUTEURS VALUES('Hazanavicius','Michel','1967-03-29','français');
INSERT INTO CONTRIBUTEURS VALUES('Goldman','Jean-Jacques','1951-10-11','français');
INSERT INTO CONTRIBUTEURS VALUES('Crozat','Stephane','1975-09-10','français');
INSERT INTO CONTRIBUTEURS VALUES('Shelley','Mary','1797-08-30','britannique');

INSERT INTO ROLES VALUES('Mozart','Amadeus','1756-01-27','Compositeur');
INSERT INTO ROLES VALUES('Orwell','George','1903-06-25','Auteur');
INSERT INTO ROLES VALUES('Tarantino','Quentin','1963-03-27','Realisateur');
INSERT INTO ROLES VALUES('Dujardin','Jean','1972-06-19','Acteur');
INSERT INTO ROLES VALUES('Hazanavicius','Michel','1967-03-29','Realisateur');
INSERT INTO ROLES VALUES('Goldman','Jean-Jacques','1951-10-11','Compositeur');
INSERT INTO ROLES VALUES('Goldman','Jean-Jacques','1951-10-11','Interprete');
INSERT INTO ROLES VALUES('Crozat','Stephane','1975-09-10','Auteur');
INSERT INTO ROLES VALUES('Shelley','Mary','1797-08-30','Auteur');

-- Ajout des ressources (dans la table ressources et les tables filles correspondantes) et des exemplaires

INSERT INTO Ressource (titre,date,editeur,genre,code_classification) VALUES ('La flute enchantee','1791-01-01',NULL,'Musique classique','MOZ');
INSERT INTO Ressource (titre,date,editeur,genre,code_classification) VALUES ('OSS 117 : Le Caire, nid d''espions','2006-04-11','Mandarin Cinema','Comedie','OSS');
INSERT INTO Ressource(titre,date,editeur,genre,code_classification) VALUES ('1984','1749-01-01','Gallimard','Dystopie','ORW');
INSERT INTO Ressource (titre,date,editeur,genre,code_classification) VALUES ('Envole moi','1984-01-01','Maison de disque','Variete francaise','GOL');
INSERT INTO EnregMusic VALUES (4,222);
INSERT INTO Ressource (titre,date,editeur,genre,code_classification) VALUES ('Django UNCHAINED','2013-01-16','Sony Pictures','Action','DJA');
INSERT INTO Ressource (titre,date,editeur,genre,code_classification) VALUES ('Pulp Fiction','1994-10-26','Bac Films','Action','PUL');
INSERT INTO Ressource(titre,date,editeur,genre,code_classification) VALUES ('Frankenstein','2009-01-01','Le Livre de Poche','Horreur','SHE');
INSERT INTO Ressource(titre,date,editeur,genre,code_classification) VALUES ('Traces','2018-06-01','Framabook','Intelligence Artificielle','CRO');

INSERT INTO EnregMusic VALUES (1,9900);
INSERT INTO Film VALUES (2,5940,'En 1955, Le Caire est un véritable nid d''espions. Tout le monde se méfie de tout le monde : Anglais, Français, Soviétiques, la famille du roi déchu Farouk qui veut retrouver son trône, les Aigles de Kheops, secte religieuse qui veut prendre le pouvoir.','Francais');
INSERT INTO Livre VALUES (3,'9782072730030','En 1984, le monde, depuis les grandes guerres nucléaires des années 1950, est divisé en trois grands « blocs » : Océania, Eurasia, Estasia. Ces trois grandes puissances sont dirigées par différents régimes totalitaires revendiqués comme tels, et s''appuyant sur des idéologies nommées différemment mais fondamentalement similaires. L’histoire est celle de Winston Smith, employé du régime d’Océania..','français');
INSERT INTO Film VALUES (5,9900,'Un ancien esclave s''associe avec un chasseur de primes d''origine allemande qui l''a libéré : il accepte de traquer avec lui des criminels recherchés. En échange, il l''aidera à retrouver sa femme perdue depuis longtemps et esclave elle-aussi. Un western décoiffant.','English');
INSERT INTO Film VALUES (6,10680,'L''odyssée sanglante et burlesque de petits malfrats dans la jungle de Hollywood à travers trois histoires qui s''entremêlent. Dans un restaurant, un couple de jeunes braqueurs, Pumpkin et Yolanda, discutent des risques que comporte leur activité. Deux truands, Jules Winnfield et son ami Vincent Vega','English');
INSERT INTO Livre VALUES (7,'9782253088752','Le livre raconte l''histoire de Victor Frankenstein, un médecin qui est fasciné par la vie et par la Création. Il décide de créer à son tour une créature vivante et de l''animer, afin d''essayer de comprendre comment fonctionne la vie et comment elle a été créée. Malheureusement, la Créature, dotée de sa propre personnalité, échappe à tout contrôle, et s''enfuit.','français');
INSERT INTO Livre VALUES (8,'9791092674224','Vivre et mourir au temps des IA','français');

INSERT INTO Exemplaire VALUES(1,1,'Bon');
INSERT INTO Exemplaire VALUES(1,2,'Neuf');
INSERT INTO Exemplaire VALUES(1,3,'Neuf');
INSERT INTO Exemplaire VALUES(2,1,'Mauvais');
INSERT INTO Exemplaire VALUES(3,1,'Neuf');
INSERT INTO Exemplaire VALUES(3,2,'Bon');
INSERT INTO Exemplaire VALUES(3,3,'Mauvais');
INSERT INTO Exemplaire VALUES(3,4,'Perdu');
INSERT INTO Exemplaire VALUES(4,1,'Bon');
INSERT INTO Exemplaire VALUES(5,1,'Neuf');
INSERT INTO Exemplaire VALUES(6,1,'Bon');
INSERT INTO Exemplaire VALUES(7,1,'Bon');
INSERT INTO Exemplaire VALUES(8,1,'Neuf');

-- Ajout des contributions 

INSERT INTO ContribLivre VALUES(3,'Orwell','George','1903-06-25','Auteur');
INSERT INTO ContribLivre VALUES(7,'Shelley','Mary','1797-08-30','Auteur');
INSERT INTO ContribLivre VALUES(8,'Crozat','Stephane','1975-09-10','Auteur');

INSERT INTO ContribFilm VALUES(2,'Hazanavicius','Michel','1967-03-29','Realisateur');
INSERT INTO ContribFilm VALUES(2,'Dujardin','Jean','1972-06-19','Acteur');
INSERT INTO ContribFilm VALUES(5,'Tarantino','Quentin','1963-03-27','Realisateur');
INSERT INTO ContribFilm VALUES(6,'Tarantino','Quentin','1963-03-27','Realisateur');

INSERT INTO ContribMusic VALUES(1,'Mozart','Amadeus','1756-01-27','Compositeur');
INSERT INTO ContribMusic VALUES(4,'Goldman','Jean-Jacques','1951-10-11','Compositeur');
INSERT INTO ContribMusic VALUES(4,'Goldman','Jean-Jacques','1951-10-11','Interprete');


-- Ajout des personnes (membres et adherents)

INSERT INTO Personne VALUES ('aubardlu','Aubard','Lucas','41 rue de la croix','lucas.aubard@gmail.com','1234');
INSERT INTO Personne VALUES ('wacqueza','Wacquez','Arthur','20 rue de Chateauroux','arthur.wacquez@gmail.com','2345');
INSERT INTO Personne VALUES ('toczeko','Tocze','Korantin','56 rue de margny','korantin.tocze@gmail.com','3456');
INSERT INTO Personne VALUES ('aubardma','Aubard','Martin','43 rue de la croix','martin.aubard@gmail.com','5678');
INSERT INTO Personne VALUES ('joslinb','Bouteau','Joslin','02 rue de saint jean','joslin.bouteau@gmail.com','2589');
INSERT INTO Personne VALUES ('vladimirp','Poutine','Vladimir','02 rue de moscou','vladimir.poutine@gmail.com','1025');
INSERT INTO Personne VALUES ('rousseaut','Rousseau','Tamara','36 bis allee du verger','rousseau.tamara@gmail.com','2589');

INSERT INTO Membre VALUES ('aubardlu');
INSERT INTO Membre VALUES ('wacqueza');

INSERT INTO Adherent VALUES ('toczeko','1995-02-05','0603050809',FALSE);
INSERT INTO Adherent VALUES ('aubardma','1992-11-06','0695457612',FALSE);
INSERT INTO Adherent VALUES ('vladimirp','1964-09-16','0784965212',TRUE);
INSERT INTO Adherent VALUES ('rousseaut', '1956-04-18','0259784123',FALSE);

-- Ajout des adhesions 

INSERT INTO Adhesion VALUES('vladimirp','2012-03-28','2013-03-28');
INSERT INTO Adhesion VALUES('toczeko','2018-01-01','2019-01-01');
INSERT INTO Adhesion VALUES('toczeko','2019-01-01','2020-01-01');
INSERT INTO Adhesion VALUES('toczeko','2020-01-01','2021-01-01');
INSERT INTO Adhesion VALUES('aubardma','2012-04-05','2013-04-05');
INSERT INTO Adhesion VALUES('aubardma','2019-01-01','2020-01-01');
INSERT INTO Adhesion VALUES('aubardma','2020-01-01','2021-01-01');
INSERT INTO Adhesion VALUES('rousseaut','2019-05-04','2020-05-04');

-- Ajout des prets et sanctions

INSERT INTO Pret VALUES ('vladimirp',3,4,'2012-03-28','2012-05-25','Bon','Perdu');
INSERT INTO Pret VALUES ('vladimirp',1,1,'2012-03-28','2012-05-25','Neuf','Bon');
INSERT INTO Pret VALUES ('toczeko',3,2,'2019-12-05','2019-12-25','Neuf','Bon');
INSERT INTO Pret VALUES('toczeko',1,3,'2020-03-01',NULL,'Bon',NULL);
INSERT INTO Pret VALUES ('toczeko',3,2,'2020-03-25',NULL,'Bon',NULL);
INSERT INTO Pret VALUES ('aubardma',1,2,'2020-03-27',NULL,'Neuf',NULL);
INSERT INTO Pret VALUES ('aubardma',1,1,'2020-03-27',NULL,'Bon',NULL);
INSERT INTO Pret VALUES ('rousseaut',5,1,'2020-03-28',NULL,'Neuf',NULL);
INSERT INTO Pret VALUES ('rousseaut',6,1,'2020-03-28',NULL,'Bon',NULL);

INSERT INTO Sanction VALUES(3,4,'vladimirp','2012-05-25','Perte de la ressource');
INSERT INTO Sanction VALUES(1,1,'vladimirp','2012-05-25','Retard');

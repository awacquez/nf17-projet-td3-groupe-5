# NF17 Projet - TD3 Groupe 5 

Projet de l'UV NF17 en P20  
TD D3 - Groupe 5  
WACQUEZ Arthur  
AUBARD Lucas  
TOCZE Korantin  
Hanlin WU

## Note sur les versions  
Le projet contient 2 dossiers :  
- Le dossier V1 correspond aux premières versions des documents, réalisées avant la conception du MLD
- Le dossier V2 correspond aux rétroconceptions des documents, une fois la conception du MLD réalisée.
